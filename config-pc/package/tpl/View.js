/**
 * 项目view
 */
/* eslint-disable */
Ext.define("{class}", {
  extend: 'Ext.panel.Panel',
  alias:'widget.{xtype}',
  border: 0,
  layout: 'fit',
  autoScroll: true,
  initComponent: function () {
    var me = this;
    me.bodyStyle = "background-color: #f0f2f5;";
    me.html = '<div class="loading" style="color:#c7c7c7;text-align:center;padding-top:18%;">' + JE.getLocalLang('common.loading') + '</div>';
    me.callParent(arguments);
    me.on('activate',function(){
      if (me.created){
        me.load();
      }else{
        me.created = true;
      }
    })
  },
  afterRender: function () {
    var me = this;
    me.callParent(arguments);
    var folder = '{entry}'.split('/').pop();
    var tag = folder.replace(/([A-Z])/g, '-$1').toLowerCase();
    var vueInfo = me.vueInfo || {}
    //加载本页面资源
    JE.loadScript([
      '/static/ux/moment/moment.min.js',
      '/static/vue/'+folder+'/index.js',
      '/static/vue/'+folder+'/index.css',
    ], function () {
      //时间组件
      if (window.moment) {
        if (window._JE_LOCAL_LANG == 'zh_CN') {
          window.moment.locale('zh-CN')
        } else {
          window.moment.locale('en')
        }
      }
      var loading = me.body.down('.loading');
      loading && loading.remove();
      //创建vue装载dom
      var dom = me.body.insertHtml('beforeEnd', '<' + tag +' :callback="callback" :params="params"/>');
      //载入页面
      me.vm = new Vue({
        el: dom,
        data: function () {
          return {
            params: vueInfo.params
          }
        },
        methods: {
          callback: vueInfo.callback
        }});
    },true);

  },
  load:function(){
    var me=this;
    var loadArg=arguments;
    JE.defer(function(){
      var vm = me.getVM();
      vm && vm.load && vm.load.apply(vm,loadArg);
      if(vm || !me.rendered){
        return false;
      }
    },100);
  },
  setReadOnly:function(){
    var me=this;
    var loadArg=arguments;
     JE.defer(function(){
      var vm = me.getVM();
      vm && vm.setReadOnly && vm.setReadOnly.apply(vm,loadArg);
      if(vm || !me.rendered){
        return false;
      }
    },100);
  },
  getVM:function(){
    if(this.vm && this.vm.$children && this.vm.$children.length>0){
      return this.vm.$children[0];
    }
  }
})
