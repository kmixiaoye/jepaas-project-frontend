/**
 * ajax请求
 * @param {String} url 请求链接
 * @param {String} params 请求参数
 * @param {Object} [config] axios配置项
 * @returns {Promise}
 */
export default function fetch(...args) {
  return JE.fetch(...args);
}
