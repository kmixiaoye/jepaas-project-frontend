/**
 * @Author : ZiQin Zhai
 * @Date : 2020/1/16 15:20
 * @Version : 1.0
 * @Last Modifined by : ZiQin Zhai
 * @Last Modifined time : 2020/1/16 15:20
 * @Description sql构建器
 * */

// 表达式取值
export const EXP_TYPE = {
  in: 'in',
  inSelect: 'inSelect',
  notIn: 'notIn',
  notInSelect: 'notInSelect',
  notNull: 'noNull',
  isNull: 'isNull',
  between: 'between',
  ne: '!=',
  gt: '>',
  ge: '>=',
  lt: '<',
  le: '<=',
  likeLeft: '%like',
  likeRight: 'like%',
  like: 'like',
  eq: '=',
  and: 'and',
  or: 'or',
};
export const ORDER_TYPE = {
  asc: 'ASC',
  desc: 'DESC',
};
export const FORM_DATA_TYPE = {
  select: 'select',
  child: 'child',
};


export default JE.safetySqlUtil;
