
export function windowDimensions() {
  const { height } = window.screen;
  return height;
}
export function getRem() {
  const { flexible: { dpr } } = window.lib;
  const doc = window.document;
  const docEl = doc.documentElement;
  let { width } = docEl.getBoundingClientRect();
  if (width / dpr > 540) {
    width = 540 * dpr;
  }
  const rem = width / 10;
  return rem;
}
export function px2rem(px) {
  const rem = getRem();
  const val = parseFloat(px) / rem;
  // if (typeof px === 'string' && px.match(/px$/)) {
  //   val += 'rem';
  // }
  return val;
}

export function isApplicationExist(app) {
  switch (app) {
    case 'weixin':
      return plus.runtime.isApplicationExist({ pname: 'com.tencent.mm', action: 'weixin://' }) && JE.getApkConfig('JE_SHOW_WX') == 1;

    case 'qq':
      return plus.runtime.isApplicationExist({ pname: 'com.tencent.mobileqq', action: 'mqqapi://' }) && JE.getApkConfig('JE_SHOW_QQ') == 1;

    default:
      return false;
  }
}
