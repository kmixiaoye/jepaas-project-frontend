/**
 *获取公告信息
 * @param param
 * @returns {Promise<T | never>}
 */
export function getComInfo (params) {
  return JE.ajax({url:'/je/getInfoById',params}).then(data => data)
    .catch();
}
