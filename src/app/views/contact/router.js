/**
 * @Author : ZiQin Zhai
 * @Date : 2020/10/9 13:40
 * @Version : 1.0
 * @Last Modifined by : ZiQin Zhai
 * @Last Modifined time : 2020/10/9 13:40
 * @Description
 * */
import Index from './index.vue';
import rootPage from './rootPage';

export default [
  {
    path: '/JE-PLUGIN-CONTACT',
    name: 'JE-PLUGIN-CONTACT',
    component: rootPage,
    children: [
      {
        path: '/',
        component: Index,
      }
    ],
  }
];
