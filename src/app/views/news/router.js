/**
 * @Author : ZiQin Zhai
 * @Date : 2020/10/9 13:40
 * @Version : 1.0
 * @Last Modifined by : ZiQin Zhai
 * @Last Modifined time : 2020/10/9 13:40
 * @Description
 * */
import Index from './index.vue';
import info from './pages/info/index.vue';
import rootPage from './rootPage';

export default [
  {
    path: '/JE-PLUGIN-NEWS',
    name: 'JE-PLUGIN-NEWS',
    component: rootPage,
    children: [
      {
        path: '/',
        component: Index,
      },
      {
        path:"info",
        component: info,
      }
    ],
  }
];
