
import Index from './index.vue';
import rootPage from './rootPage.vue';
// 添加页面
import addtransaction from './pages/addtransaction/index.vue';
// 详情页面
import detail from './pages/detail/index.vue';
// 搜索页面
import searchpage from './pages/searchpage/index.vue';

export default [
  {
    path: '/JE-PLUGIN-TRANSACTION',
    component: rootPage,
    redirect: '/JE-PLUGIN-TRANSACTION/index',
    children: [
      {
        path: '',
        component: Index,
      },
      {
        path: 'list',
        name: 'list',
        component: Index,
      },
      {
        path: 'addtransaction',
        component: addtransaction,
      },
      {
        path: 'detail',
        component: detail,
      },
      {
        path: 'searchpage',
        component: searchpage,
      },
    ],
  },
];
