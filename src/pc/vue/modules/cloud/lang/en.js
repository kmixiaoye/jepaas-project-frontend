export const m = {
  weiyou_create: 'create', // 新建微邮
  weiyou_unknow: 'unknow', // 未知
  weiyou_nocontent: 'No content', // 无内容
  weiyou_download: 'Download', // 下载
  weiyou_preview: 'Preview', // 预览
  weiyou_newail: 'New micro mail', // 新建微邮
  weiyou_recipients: 'Recipients', // 收件人
  weiyou_pleaseschoose: 'Please choose', // 请选择
  weiyou_setmastersender: 'Set the master sender', // 设置主送人
  weiyou_theme: 'Theme', // 主题
  weiyou_content: 'Content', // 正文
  weiyou_accessory: 'Accessory', // 附件
  weiyou_emoji: 'Emoji', // 表情
  weiyou_send: 'Send', // 发 布
  weiyou_sendcomment: 'Send Comment',
  weiyou_cancel: 'Cancel',
  weiyou_fullscreen: 'Fullscreen',
  weiyou_send1: 'Send', // 发 布
  weiyou_close: 'Close', // 关 闭
  weiyou_modification: 'Modification', // 修改
  weiyou_delete: 'Delete', // 删除
  weiyou_addresser: 'Addresser', // 发件人
  weiyou_time: 'Time', // 时间
  weiyou_like: 'Like', // 点赞
  weiyou_read: 'Read', // 已阅
  weiyou_unread: 'Unread', // 未阅
  weiyou_nouploaded: 'No attachment uploaded', // 没有上传附件
  weiyou_comments: 'Comments', // 评论
  weiyou_reply: 'Reply', // 回复
  weiyou_nosubjectcontent: ' No subject content', // 暂无主题内容
  weiyou_nocomment: 'No Comment', // 暂无评论内容
  weiyou_keyword: 'Keyword', // 关键字
  weiyou_date: 'Date', // 日期
  weiyou_nametheme: 'Name/Theme', // 姓名/主题
  weiyou_begingdate: 'Begin', // 开始日期
  weiyou_enddate: 'End', // 结束日期
  weiyou_selected: 'Selected', // 已选
  weiyou_sure: 'Sure', // 确 定
  weiyou_allremind: 'All remind', // 全部提醒
  weiyou_requirements: 'Upload format requirements: up to 6 files, size 30M', // 上传格式要求： 最多支持上传6个文件，大小30M
};
