/* eslint-disable new-cap */
/*
 * @Descripttion:
 * @version:
 * @Author: qinyonglian
 * @Date: 2019-11-04 14:52:24
 * @LastEditors: qinyonglian
 * @LastEditTime: 2019-11-22 18:14:06
 */
import BaseModel from './baseModel';

export default class recoveryModel extends BaseModel {
  constructor(option) {
    super(option);// 相当于获得父类的this指向,继承属性
    this._init(option);
  }


  _init(option) {
    // 如果是自己的私有属性
    this.deleteTime = option.deleteTime; // 删除时间
    this.id = option.id;
    this.operation = {
      text: '',
      icon: 'jeicon-trash-o',
    };
    this.docFunc = [
      { icon: 'jeicon-reduction', clickText: 'recovery' },
      { icon: 'jeicon-trash-o', clickText: 'remove' },
    ];
  }

  /*
   * 创建对象
   */
  static create(options) {
    return new recoveryModel(options);
  }
}
