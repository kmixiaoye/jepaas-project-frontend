/* eslint-disable import/prefer-default-export */
const companyAndOneself = [
  {
    text: '文件名',
    icon: true,
    span: 10,
    type: 'name',
    sort: true,
    size: true,
  },
  {
    text: '大小',
    icon: true,
    span: 4,
    type: 'size',
    sort: true,
    size: true,
  },
  {
    text: '创建人',
    icon: false,
    span: 3,
  },
  {
    text: '修改时间',
    icon: false,
    span: 4,
  },
  {
    text: '操作',
    icon: false,
    span: 3,
  },
];

const oneselfTitle = [
  {
    text: '文件名',
    icon: true,
    span: 10,
    type: 'name',
    sort: true,
    size: true,
  },
  {
    text: '大小',
    icon: true,
    span: 4,
    type: 'size',
    sort: true,
    size: true,
  },
  {
    text: '创建人',
    icon: false,
    span: 4,
  },
  {
    text: '修改时间',
    icon: false,
    span: 5,
  },
];

const shareTitle = [
  {
    text: '文件名',
    icon: true,
    span: 10,
    type: 'name',
    sort: true,
    size: true,
  },
  {
    text: '大小',
    icon: true,
    span: 4,
    type: 'size',
    sort: true,
    size: true,
  },
  {
    text: '分享人',
    icon: false,
    span: 4,
  },
  {
    text: '分享时间',
    icon: false,
    span: 4,
  },
];

const receiveTitle = [
  {
    text: '文件名',
    icon: true,
    span: 10,
    type: 'name',
    sort: true,
    size: true,
  },
  {
    text: '大小',
    icon: true,
    span: 4,
    type: 'size',
    sort: true,
    size: true,
  },
  {
    text: '分享人',
    icon: false,
    span: 4,
  },
  {
    text: '接收时间',
    icon: false,
    span: 4,
  },
];

const uploadingTitle = [
  {
    text: '文件名',
    icon: true,
    span: 10,
    type: 'name',
    sort: true,
    size: true,
  },
  {
    text: '大小',
    icon: true,
    span: 4,
    type: 'size',
    sort: true,
    size: true,
  },
  {
    text: '上传目录',
    icon: false,
    span: 4,
  },
  {
    text: '状态',
    icon: false,
    span: 4,
  },
];

const recordTitle = [
  {
    text: '文件名',
    icon: true,
    span: 10,
    type: 'name',
    sort: true,
    size: true,
  },
  {
    text: '大小',
    icon: true,
    span: 4,
    type: 'size',
    sort: true,
    size: true,
  },
  {
    text: '状态',
    icon: false,
    span: 4,
  },
  {
    text: '时间',
    icon: false,
    span: 4,
  },
];


const recycleTitles = [
  {
    text: '文件名',
    icon: true,
    span: 10,
    type: 'name',
    sort: true,
    size: true,
  },
  {
    text: '大小',
    icon: true,
    span: 4,
    type: 'size',
    sort: true,
    size: true,
  },
  {
    text: '删除时间',
    icon: false,
    span: 4,
  },
];

/*
  根据路由参数改变数据显示,根据左侧菜单点击切换内容的数据
 */
export function menuChangeData(router) {
  let obj = {};
  switch (router) {
    case '1-1':
      obj = { title: companyAndOneself };
      break;
    case '1-2':
      obj = { title: oneselfTitle };
      break;
    case '1-3':
      obj = { title: shareTitle };
      break;
    case '1-4':
      obj = { title: receiveTitle };
      break;
    case '2':
      obj = { title: uploadingTitle };
      break;
    case '3':
      obj = { title: recordTitle };
      break;
    case '5':
      obj = { title: recycleTitles };
      break;
    default:
      break;
  }
  return obj;
}
