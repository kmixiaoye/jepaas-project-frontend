/*
 * @Description:
 * @version: V1.0.0
 * @Author: Shuangshuang Song
 * @Date: 2020-06-17 11:20:00
 * @LastEditTime: 2020-08-11 16:40:37
 * @LastEditors: Shuangshuang Song
 */
const m = {
  alert1: 'Empty content cannot be sent', // 不可以发送空内容
  alert2: 'Input more than 60 words', // 输入内容超过60字
  alert4: 'Input more than 600 words',
  alert5: 'The comments exceeded 500 words',
  alert3: 'Post success', // 冒泡成功
  seletesuccessfully: 'Delete successfully', // 删除成功
  contentcantempty: 'The content can\'t be empty', // 内容不能为空哦
  commentssuccessfully: 'Successful comments', // 发布评论成功
  commentsfail: 'Failure to comment', // 发布评论失败
  pictureundefine: 'The picture does not exist', // 图片不存在
  trylater: 'Frequent operation, please try again later', // 操作频繁，请稍后再试
  modifiedsuccessfully: 'Modified successfully', // 修改成功
  needsaveit: 'The content has been changed, do you need to save it', // 内容有改动，需要保存吗
  savesuccessfully: 'Saved successfully', // 保存成功
  uploadmaxnum: 'The maximum number of uploads has been reached. Please delete it before uploading', // 已达到最大上传个数，请删除后在进行上传
};

export default m;
