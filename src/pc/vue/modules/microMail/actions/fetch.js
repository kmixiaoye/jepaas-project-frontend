/**
 * ajax请求
 * @param {String} url 请求链接
 * @param {String} params 请求参数
 * @param {Object} [cfg] ajax配置项
 * @returns {Promise}
 */
export default function fetch(url, params, cfg = {}) {
  const config = {
    method: 'post',
    url,
    params: params.params || params,
    data: params.data || params,
    async: cfg.async,
    timeout: cfg.timeout,
  };
  // 增加页面调试参数
  return new Promise((resolve, reject) => {
    JE.ajax({
      ...config,
      success(response) {
        resolve(JE.getAjaxData(response));
      },
      failure(response) {
        JE.error(response.responseText, config.url, JE.format(JE.local.lang.core.util.Util.ajaxError, Ext.encode(config.params)));
        reject();
      },
    });
  });
}
