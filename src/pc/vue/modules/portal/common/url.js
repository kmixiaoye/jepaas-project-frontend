const HOST = '';
// 门户查询
export const POST_PAGE = `${HOST}/je/journalism/mhList`;
// 一级板块查询
export const POST_LEVEL_ONE = `${HOST}/je/journalism/yjbkList`;
// 二级板块查询
export const POST_LEVEL_SEC = `${HOST}/je/journalism/ejbkList`;
// 轮播图查询
export const POST_CAROUSEL = `${HOST}/je/journalism/lbtList`;
// 新闻查询
export const POST_SEARCHNEWS = `${HOST}/je/journalism/xwList`;
// 新闻详情
export const POST_NEWSDETAIL = `${HOST}/je/journalism/xwDetails`;
// 重点关注
export const POST_FOUSE = `${HOST}/je/journalism/zdgzghList`;
// 点赞
export const POST_DOZAN = `${HOST}/je/journalism/addLike`;
// 查询点赞接口
export const POST_ZANNUM = `${HOST}/je/journalism/getLikeList`;
// 评论查询
export const POST_COMMENT = `${HOST}/je/journalism/getXwComment`;
// 写入评论
export const POST_ADDCOMMENT = `${HOST}/je/journalism/addXwComment`;
// 删除评论
export const POST_DELETECOMMENT = `${HOST}/je/journalism/deleteLike`;
// 获取单个新闻
export const POST_SINGLENEWS = `${HOST}/je/journalism/getXwBean`;
