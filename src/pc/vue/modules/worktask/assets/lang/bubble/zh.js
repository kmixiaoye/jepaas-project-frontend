const m = {
  bubbleup: '冒个泡吧亲',
  sayholder: '说你想说的(60字以内)...',
  sayholder1: '说你想说的(600字以内)...',
  maopao: '冒泡',
  maopao1: '冒 泡',
  maopao2: '冒泡',
  zuixinmaopao: '最新冒泡',
  zuiremaopao: '最热冒泡',
  maopaoxiangqing: '冒泡详情',
  comment: '评论',
  somethingsay: '我有话要说(500字以内)...',
  xuanzebiaoqing: '选择表情',
  xuanzetupian: '选择图片',
  remenmaopao: '热门冒泡',
  floor: '楼',
  likes: '点赞',
  delete: '删除',
  reply: '回复',
};

export default m;
